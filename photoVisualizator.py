import tkinter as tk  # Импорт модуля tkinter для создания графического интерфейса
from tkinter import filedialog, colorchooser  # Импорт модулей для работы с диалоговыми окнами выбора файлов и цветов
from PIL import Image, ImageTk, ImageFilter, ImageOps  # Импорт модулей для работы с изображениями

class ImageEditor:  # Определение класса ImageEditor
    def __init__(self, root):  # Инициализация класса
        self.root = root  # Определение корневого окна
        self.root.geometry("1000x800")  # Установка размеров окна
        self.original_image = None  # Изначальное изображение
        self.modified_image = None  # Измененное изображение
        self.tint_color = None  # Цвет оттенка

        self.create_widgets()  # Создание виджетов

    def create_widgets(self):  # Функция для создания виджетов
        self.open_button = tk.Button(self.root, text="Открыть изображение", command=self.open_image)  # Кнопка для открытия изображения
        self.open_button.pack()  # Размещение кнопки на экране

        self.apply_effect_button = tk.Button(self.root, text="Добавить негатив", command=self.negative_photo)  # Кнопка для применения эффекта негатива
        self.apply_effect_button.pack()  # Размещение кнопки на экране

        self.apply_effect_blur = tk.Button(self.root, text="Добавить блюр на фото", command=self.apply_effect_blur)  # Кнопка для применения эффекта размытия
        self.apply_effect_blur.pack()  # Размещение кнопки на экране

        self.tint_button = tk.Button(self.root, text="Изменить цвет", command=self.apply_tint)  # Кнопка для изменения цвета
        self.tint_button.pack()  # Размещение кнопки на экране

        self.rotate_button = tk.Button(self.root, text="Эффект повернуть на 90°", command=self.apply_rotate_effect)  # Кнопка для поворота изображения на 90 градусов
        self.rotate_button.pack()  # Размещение кнопки на экране

        self.sepia_button = tk.Button(self.root, text="Эффект сепии", command=self.apply_effect_sepia)  # Кнопка для применения эффекта сепии
        self.sepia_button.pack()  # Размещение кнопки на экране

        self.save_button = tk.Button(self.root, text="Сохранить", command=self.save_image)  # Кнопка для сохранения изображения
        self.save_button.pack()  # Размещение кнопки на экране

        self.image_label = tk.Label(self.root)  # Метка для отображения изображения
        self.image_label.pack()  # Размещение метки на экране

    def apply_tint(self):  # Функция для применения оттенка
        if self.modified_image:  # Если изображение было изменено
            color = colorchooser.askcolor(title="Выберите цвет")  # Запрос цвета у пользователя
            if color:  # Если цвет был выбран
                self.tint_color = color[1]  # Сохранение выбранного цвета
                self.modified_image = self.add_tint(self.modified_image, self.tint_color)  # Применение оттенка к изображению
                self.display_image(self.modified_image)  # Отображение измененного изображения

    def add_tint(self, image, tint_color):  # Функция для добавления оттенка к изображению
        r, g, b = image.split()  # Разделение изображения на каналы
        r = r.point(lambda i: i * (int(tint_color[1:3], 16) / 255))  # Применение оттенка к красному каналу
        g = g.point(lambda i: i * (int(tint_color[3:5], 16) / 255))  # Применение оттенка к зеленому каналу
        b = b.point(lambda i: i * (int(tint_color[5:], 16) / 255))  # Применение оттенка к синему каналу
        return Image.merge('RGB', (r, g, b))  # Слияние каналов обратно в изображение

    def open_image(self):  # Функция для открытия изображения
        filename = filedialog.askopenfilename(filetypes=(("PNG files", "*.png"), ("JPEG/JPG files", "*.jpg;*.jpeg"), ("All files", "*.*")))  # Запрос файла у пользователя
        if filename:  # Если файл был выбран
            self.original_image = Image.open(filename)  # Открытие изображения
            self.modified_image = self.original_image.copy()  # Создание копии изображения для модификации
            self.display_image(self.modified_image)  # Отображение изображения

    def negative_photo(self):  # Функция для применения эффекта негатива
        if self.modified_image:  # Если изображение было изменено
            self.modified_image = ImageOps.invert(self.modified_image)  # Применение эффекта негатива
            self.display_image(self.modified_image)  # Отображение измененного изображения

    def apply_effect_blur(self):  # Функция для применения эффекта размытия
        if self.modified_image:  # Если изображение было изменено
            self.modified_image = self.modified_image.filter(ImageFilter.GaussianBlur(30.0))  # Применение эффекта размытия
            self.display_image(self.modified_image)  # Отображение измененного изображения

    def apply_rotate_effect(self):  # Функция для применения эффекта поворота
        if self.modified_image:  # Если изображение было изменено
            self.modified_image = self.modified_image.rotate(90, expand=True)  # Поворот изображения на 90 градусов
            self.display_image(self.modified_image)  # Отображение измененного изображения

    def apply_effect_sepia(self):  # Функция для применения эффекта сепии
        if self.modified_image:  # Если изображение было изменено
            gray = self.modified_image.convert("L")  # Конвертация изображения в оттенки серого
            self.modified_image = Image.merge(  # Слияние каналов обратно в изображение
                "RGB",
                (
                    gray.point(lambda x: x * 240 / 255),  # Применение эффекта сепии к красному каналу
                    gray.point(lambda x: x * 200 / 255),  # Применение эффекта сепии к зеленому каналу
                    gray.point(lambda x: x * 145 / 255)  # Применение эффекта сепии к синему каналу
                )
            )
        self.display_image(self.modified_image)  # Отображение измененного изображения

    def save_image(self):  # Функция для сохранения изображения
        if self.modified_image:  # Если изображение было изменено
            filename = filedialog.asksaveasfilename(defaultextension=".png",  # Запрос имени файла у пользователя
                                                    filetypes=(
                                                        ("PNG files", "*.png"),  # Форматы файлов для сохранения
                                                        ("All files", "*.*")))
            if filename:  # Если имя файла было выбрано
                self.modified_image.save(filename)  # Сохранение изображения

    def display_image(self, image):  # Функция для отображения изображения
        width, height = image.size  # Получение размеров изображения

        # Вычисляем коэффициент сжатия,
        # чтобы бoльшая сторона была не более 600 пикселей
        if width > height:  # Если ширина больше высоты
            if width > 600:  # Если ширина больше 600 пикселей
                ratio = 600 / width  # Вычисление коэффициента сжатия
            else:
                ratio = 1  # Если ширина меньше или равна 600 пикселей, коэффициент сжатия равен 1
        else:  # Если высота больше ширины
            if height > 600:  # Если высота больше 600 пикселей
                ratio = 600 / height  # Вычисление коэффициента сжатия
            else:
                ratio = 1  # Если высота меньше или равна 600 пикселей, коэффициент сжатия равен 1
        image = image.resize(
            (int(width * ratio), int(height * ratio)))  # Изменение размера изображения с учетом коэффициента сжатия
        photo = ImageTk.PhotoImage(image)  # Создание объекта PhotoImage из изображения
        self.image_label.config(image=photo)  # Установка изображения в метку
        self.image_label.image = photo  # Сохранение ссылки на изображение

root = tk.Tk()  # Создание корневого окна
root.title("Image Editor")  # Установка заголовка окна
editor = ImageEditor(root)  # Создание объекта ImageEditor
root.mainloop()  # Запуск главного цикла обработки событий
